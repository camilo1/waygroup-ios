//
//  PopUpViewController.h
//  WayGroup
//
//  Created by Apnovator-Prakshi on 1/3/15.
//  Copyright (c) 2015 Apnovator-Prakshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopUpViewController : UIViewController
{
    IBOutlet UIView *viewWin;
}
-(IBAction)CloseBtnClick:(id)sender;
@end
