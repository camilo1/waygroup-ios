//
//  UserOneReportViewController.m
//  WayGroup
//
//  Created by Apnovator-Prakshi on 12/31/14.
//  Copyright (c) 2014 Apnovator-Prakshi. All rights reserved.
//

#import "UserOneReportViewController.h"
#import "TakePictureViewController.h"
#import "SeeOneByOneViewController.h"
#import "WGAppDelegate.h"

@interface UserOneReportViewController ()

@end

@implementation UserOneReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
   WGAppDelegate *app = (WGAppDelegate *)[UIApplication sharedApplication].delegate;
    [app getImage:app.userName];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)back:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(IBAction)reporter:(id)sender
{

TakePictureViewController *tp=[[TakePictureViewController alloc]initWithNibName:nil bundle:nil];
[self.navigationController pushViewController:tp animated:YES];
}
-(IBAction)seeOneByOne:(id)sender
{
    
    SeeOneByOneViewController *so=[[SeeOneByOneViewController alloc]initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:so animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
