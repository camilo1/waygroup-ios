//
//  UserLoginViewController.m
//  WayGroup
//
//  Created by Apnovator-Prakshi on 12/30/14.
//  Copyright (c) 2014 Apnovator-Prakshi. All rights reserved.
//

#import "UserLoginViewController.h"
#import "UserOneViewController.h"

@interface UserLoginViewController ()

@end

@implementation UserLoginViewController
@synthesize tf1Str;
@synthesize tf2Str;

- (void)viewDidLoad
{
    
    username.text=tf1Str;
    password1.text=tf2Str;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)back:(id)sender
{
 
    [self.navigationController popViewControllerAnimated:YES];
    
}

/*f
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
