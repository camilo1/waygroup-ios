//
//  UserTypeViewController.m
//  WayGroup
//
//  Created by Apnovator-Prakshi on 12/30/14.
//  Copyright (c) 2014 Apnovator-Prakshi. All rights reserved.
//

#import "UserTypeViewController.h"
#import "UserOneViewController.h"
#import "UserSecondReportViewController.h"
#import "UserTwoViewController.h"
#import "TakePictureViewController.h"
@interface UserTypeViewController ()

@end

@implementation UserTypeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(IBAction)goToUserOne:(id)sender
{
    UserOneViewController *uo=[[UserOneViewController alloc]initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:uo animated:YES];
}
-(IBAction)goToUserSecond:(id)sender
{
    UserTwoViewController *ut=[[UserTwoViewController alloc]initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:ut animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
