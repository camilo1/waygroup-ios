//
//  UserSecondReportViewController.m
//  WayGroup
//
//  Created by Apnovator-Prakshi on 12/31/14.
//  Copyright (c) 2014 Apnovator-Prakshi. All rights reserved.
//

#import "UserSecondReportViewController.h"
#import "TakePictureViewController.h"
#import "ShowLocalImagesViewController.h"
@interface UserSecondReportViewController ()

@end

@implementation UserSecondReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)reporter:(id)sender
{
    
    ShowLocalImagesViewController *tp=[[ShowLocalImagesViewController alloc]initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:tp animated:YES];
}
-(IBAction)back:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(IBAction)goToSendPictures:(id)sender

{
    TakePictureViewController *tp=[[TakePictureViewController alloc]initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:tp animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
