//
//  ShowLocalImagesViewController.m
//  WayGroup
//
//  Created by Amar on 30/01/15.
//  Copyright (c) 2015 Apnovator-Prakshi. All rights reserved.
//

#import "ShowLocalImagesViewController.h"
#import "WebViewController.h"
#import "CustomCell.h"
@interface ShowLocalImagesViewController ()

@end

@implementation ShowLocalImagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     dateFormatter = [[NSDateFormatter alloc]init];
    app = (WGAppDelegate *)[UIApplication sharedApplication].delegate;
    // Do any additional setup after loading the view from its nib.
    [myTableView registerNib:[UINib nibWithNibName:@"CustomCell"bundle:nil] forCellReuseIdentifier:@"Cell"];
    imageArrayAfterFilter = [[NSMutableArray alloc]init];
    [myTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    NSMutableArray *imageArrayDefualt = [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"ImageArray"]];
    
    for (int i=0; i<imageArrayDefualt.count; i++) {
        imageArrayDataValue =[imageArrayDefualt objectAtIndex:i];
        id isUploadedObj = [imageArrayDataValue valueForKey:@"IsUploaded"];
        BOOL isUploaded;
        if(isUploadedObj != nil){
            isUploaded = [isUploadedObj boolValue];
        }
        else{
            isUploaded = YES;  //for all the images previously uploaded(If Key not exist)
        }
                            
    
        NSLog(@"%@",app.userName);
        if([[imageArrayDataValue valueForKey:@"UserName"] isEqualToString:app.userName] && (isUploaded))
        {
            [imageArrayAfterFilter addObject:imageArrayDataValue];
        }
    }
    
    [self reverseArray:imageArrayAfterFilter];

}
- (void)reverseArray:(NSMutableArray *)arr{
    if ([arr count] == 0)
        return;
    NSUInteger i = 0;
    NSUInteger j = [arr count] - 1;
    while (i < j) {
        [arr exchangeObjectAtIndex:i
                 withObjectAtIndex:j];
        
        i++;
        j--;
    }
}


-(IBAction)NamesClick_Tapped:(id)sender
{
    //    [myTableView scrollRectToVisible:CGRectMake(myTableView.frame.origin.x, myTableView.frame.origin.y, myTableView.frame.size.width, myTableView.frame.size.height+800) animated:YES];
    //  CGFloat oldTableViewHeight = myTableView.contentSize.height;
    [myTableView setContentOffset:CGPointMake(0,myTableView.contentOffset.y+446) animated:YES];
    // Reload your table view with your new messages
    
    // Put your scroll position to where it was before
    //  CGFloat newTableViewHeight = myTableView.contentSize.height;
    //  myTableView.contentOffset = CGPointMake(0, newTableViewHeight - oldTableViewHeight);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)goToHomePage:(id)sender

{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)goToWebview:(id)sender
{
    WebViewController *wb=[[WebViewController alloc]initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:wb animated:YES];
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 446;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    CustomCell *cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // If there is no cell to reuse, create a new one
    if(cell == nil)
    {
        cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
   
    }
  
    [cell.plusButton addTarget:self action:@selector(plusButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    // Configure the cell before it is displayed...
    
    
    cell.valueDescription.text =[[imageArrayAfterFilter objectAtIndex:indexPath.row] valueForKey:@"Description"];
    cell.title.text =[[imageArrayAfterFilter objectAtIndex:indexPath.row] valueForKey:@"Title"];
   cell.labelLocation.text = [[imageArrayAfterFilter objectAtIndex:indexPath.row] valueForKey:@"Location"];
    
   [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    NSString * dateFormatted = [dateFormatter stringFromDate:[[imageArrayAfterFilter objectAtIndex:indexPath.row] valueForKey:@"DateTime"]];
   cell.labelDate.text =dateFormatted;
   [dateFormatter setDateFormat:@"hh:mm a"];
    NSString * dateFormatted1 = [dateFormatter stringFromDate:[[imageArrayAfterFilter objectAtIndex:indexPath.row] valueForKey:@"DateTime"]];
    cell.labelTime.text =dateFormatted1;
   cell.ShowimageView.contentMode = UIViewContentModeScaleAspectFill;
    [cell.ShowimageView setClipsToBounds:YES];
    NSString *name = [NSString stringWithFormat:@"MyImageFolder/%@",[[imageArrayAfterFilter objectAtIndex:indexPath.row] valueForKey:@"ImageName"]];
    NSString *filepath=[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:name];
    UIImage *testImage = [UIImage imageWithContentsOfFile:filepath];
    cell.ShowimageView.image = testImage;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *name = [NSString stringWithFormat:@"MyImageFolder/%@",[[imageArrayAfterFilter objectAtIndex:indexPath.row] valueForKey:@"ImageName"]];
    NSString *filepath=[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:name];
    UIImage *testImage = [UIImage imageWithContentsOfFile:filepath];
    [self isplayBigImage : filepath];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return [imageArrayAfterFilter count];
}
-(void)isplayBigImage: (NSString *)imageData
{
    if([self iPhone6PlusDevice])
    {
        imageVC=[[ImageDisplayViewController alloc]initWithNibName:@"ImageDisplayViewController-HD" bundle:nil];
        imageVC.imageData = imageData;
        imageVC.isImageShow = YES;
    }
    else{
        if([[UIScreen mainScreen] bounds].size.height == 667.0)
        {
            imageVC=[[ImageDisplayViewController alloc]initWithNibName:@"ImageDisplayViewController-HD" bundle:nil];
            imageVC.imageData = imageData;
             imageVC.isImageShow = YES;
        }
        else{
            imageVC=[[ImageDisplayViewController alloc]initWithNibName:@"ImageDisplayViewController" bundle:nil];
             imageVC.imageData = imageData;
             imageVC.isImageShow = YES;
        }
        
    }
    imageVC.imageURL = [NSURL URLWithString:@"user"];
    imageVC.view.center = self.view.center;
    [self.view addSubview:imageVC.view];
    
}

-(BOOL)iPhone6PlusDevice{
    if ([UIScreen mainScreen].scale > 2.9) return YES;   // Scale is only 3 when not in scaled mode for iPhone 6 Plus
    return NO;
}

-(void)plusButtonClick:(UIButton*)button{
    
    // int rowIndex = button.tag;
    UIView *contentView = button.superview;
    CustomCell *cell = (CustomCell*)contentView.superview;
    popUpTitle.text = cell.title.text;
    popUpDescription.text = cell.valueDescription.text;
    popUpImageView.image = cell.ShowimageView.image;
    popUpView.hidden = false;
    
}

-(void)dismissPopUp:(UIButton*)sender{
    popUpView.hidden = true;
}

@end
