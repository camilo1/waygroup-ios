//
//  UserOneViewController.m
//  WayGroup
//
//  Created by Apnovator-Prakshi on 12/30/14.
//  Copyright (c) 2014 Apnovator-Prakshi. All rights reserved.
//

#import "UserOneViewController.h"
#import "UserLoginViewController.h"
#import "UserOneReportViewController.h"


@interface UserOneViewController ()

@end

@implementation UserOneViewController
@synthesize isCameraShown = __isCameraShown;
@synthesize popover = _popover;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    app.userType = [NSNumber numberWithInt:1];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:@"UserNameCheck_Notification"
                                               object:nil];
    [super viewDidLoad];
    
    app = (WGAppDelegate *)[UIApplication sharedApplication].delegate;
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewWillAppear:(BOOL)animated
{
if(!([[[NSUserDefaults standardUserDefaults]objectForKey:@"userType_1_Username"] length]<=0))
{
    tf1.text =[[NSUserDefaults standardUserDefaults]objectForKey:@"userType_1_Username"];
}
    if(!([[[NSUserDefaults standardUserDefaults]objectForKey:@"userType_1_Password"] length]<=0))
    {
        tf2.text =[[NSUserDefaults standardUserDefaults]objectForKey:@"userType_1_Password"];
    }
    
    app.userType = [NSNumber numberWithInt:1];
}

- (void) receiveNotification:(NSNotification *) notification
{
    app.userName = tf1.text;
     app.password = tf2.text;
  //  app.userType = @"1";
     [app DismisHud];
    [[NSUserDefaults standardUserDefaults]setObject:app.userName forKey:@"userType_1_Username"];
     [[NSUserDefaults standardUserDefaults]setObject:app.password forKey:@"userType_1_Password"];
    [[NSUserDefaults standardUserDefaults]synchronize];
  // [app getImage:tf1.text];
     app.userType = [NSNumber numberWithInt:1];
       UserOneReportViewController *ucr=[[UserOneReportViewController alloc]initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:ucr animated:YES];
    
}


-(IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(IBAction)enter:(id)sender
{
    [app getImage:@"User1"];
    
       
    UserOneReportViewController *ucr=[[UserOneReportViewController alloc]initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:ucr animated:YES];
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==tf1)
    {
        
        //UIView* view = [self.view viewWithTag:100];
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationCurveEaseOut
                         animations:^
         {
             [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-50, self.view.frame.size.width, self.view.frame.size.height)];
         }
                         completion:^(BOOL finished)
         {
             NSLog(@"Completed");
             
         }];
        
        
    }
    else if (textField==tf2)
    {
        //UIView* view = [self.view viewWithTag:100];
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationCurveEaseOut
                         animations:^
         {
             [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-50, self.view.frame.size.width, self.view.frame.size.height)];
         }
                         completion:^(BOOL finished)
         {
             NSLog(@"Completed");
             
         }];
    }
    return YES;
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^
     {
         [self.view setFrame:CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height)];
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
    
    
}
-(IBAction)dismisskey:(id)sender
{
    [sender resignFirstResponder];
}


-(IBAction)userOneReport: (id)sender
{
    NSString *usernameInDefaults = [[NSUserDefaults standardUserDefaults]objectForKey:@"userType_1_Username"];
    NSString *userPasswordInDefaults =  [[NSUserDefaults standardUserDefaults]objectForKey:@"userType_1_Password"];
    
    if(([tf1.text isEqualToString:usernameInDefaults]) && ([tf2.text isEqualToString:userPasswordInDefaults])){
        
        UserOneReportViewController *ucr=[[UserOneReportViewController alloc]initWithNibName:nil bundle:nil];
        [self.navigationController pushViewController:ucr animated:YES];
    }else{
        
        if(tf1.text.length<1 ||tf2.text.length<1)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Oops !!" message:@"Required field" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        else{
            app.userType = [NSNumber numberWithInt:1];
            [app checkUserName:tf1.text :tf2.text :[NSNumber numberWithInt:1]];
            [app ShowHud];
        }
 
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
