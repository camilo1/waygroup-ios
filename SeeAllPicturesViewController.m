//
//  SeeAllPicturesViewController.m
//  WayGroup
//
//  Created by Apnovator-Prakshi on 1/3/15.
//  Copyright (c) 2015 Apnovator-Prakshi. All rights reserved.
//

#import "SeeAllPicturesViewController.h"
#import "WGViewController.h"


@interface SeeAllPicturesViewController ()

@end

@implementation SeeAllPicturesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)goToHomePage:(id)sender

{
//    WGViewController *homePage=[[WGViewController alloc]initWithNibName:nil bundle:nil];
//    [self.navigationController pushViewController:homePage animated:YES];
     [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
