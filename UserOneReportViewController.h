//
//  UserOneReportViewController.h
//  WayGroup
//
//  Created by Apnovator-Prakshi on 12/31/14.
//  Copyright (c) 2014 Apnovator-Prakshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserOneReportViewController : UIViewController
{
    
}
-(IBAction)back:(id)sender;
-(IBAction)reporter:(id)sender;
-(IBAction)seeOneByOne:(id)sender;

@end
