//
//  ShowLocalImagesViewController.h
//  WayGroup
//
//  Created by Amar on 30/01/15.
//  Copyright (c) 2015 Apnovator-Prakshi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WGAppDelegate.h"
#import "ImageDisplayViewController.h"
@interface ShowLocalImagesViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

{
     NSDateFormatter * dateFormatter;
    IBOutlet UITableView *myTableView;
    WGAppDelegate *app;
    NSMutableArray *imageArrayAfterFilter;
    NSURL *imageURL;
    NSMutableDictionary *imageArrayDataValue;
     ImageDisplayViewController *imageVC;
    
    IBOutlet UIView *popUpView;
    IBOutlet UILabel *popUpTitle;
    IBOutlet UITextView *popUpDescription;
    IBOutlet UIImageView *popUpImageView;
}

-(IBAction)dismissPopUp:(UIButton*)sender;
-(IBAction)NamesClick_Tapped:(id)sender;

-(IBAction)goToHomePage:(id)sender;
@end
