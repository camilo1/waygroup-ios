//
//  WGViewController.h
//  WayGroup
//
//  Created by Apnovator-Prakshi on 12/29/14.
//  Copyright (c) 2014 Apnovator-Prakshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WGViewController : UIViewController<UIWebViewDelegate>
{
}

-(IBAction)dialingButtonClicked:(id)sender;
-(IBAction)goToWebview:(id)sender;

@end
