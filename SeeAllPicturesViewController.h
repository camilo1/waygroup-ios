//
//  SeeAllPicturesViewController.h
//  WayGroup
//
//  Created by Apnovator-Prakshi on 1/3/15.
//  Copyright (c) 2015 Apnovator-Prakshi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnimationExploseContainer.h"
@interface SeeAllPicturesViewController : UIViewController
{
    
}
@property (nonatomic, retain) AnimationExploseContainer *exploseContainer;
-(IBAction)goToHomePage:(id)sender;
@end
