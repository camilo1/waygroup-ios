//
//  CustomCell.h
//  SimpleTableViewController
//
//  Created by Krzysztof Satola on 07.12.2012.
//  Copyright (c) 2012 API-SOFT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell{
    
}
@property (strong, nonatomic) IBOutlet UITextView *valueDescription;
@property (strong, nonatomic) IBOutlet UILabel *labelLocation;
@property (strong, nonatomic) IBOutlet UILabel *labelDate;
@property (strong, nonatomic) IBOutlet UILabel *labelTime;
@property (strong, nonatomic) IBOutlet UIImageView *ShowimageView;
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UIButton *plusButton;


@end
