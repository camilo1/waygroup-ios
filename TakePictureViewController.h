//
//  TakePictureViewController.h
//  WayGroup
//
//  Created by Apnovator-Prakshi on 12/31/14.
//  Copyright (c) 2014 Apnovator-Prakshi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "PopUpViewController.h"

@interface TakePictureViewController : UIViewController<UIImagePickerControllerDelegate,MFMailComposeViewControllerDelegate,UITextViewDelegate ,UINavigationControllerDelegate>
{
IBOutlet UIView     *viw_EditPhoto;
    IBOutlet UIImageView  *showImage;
    IBOutlet UITextView *tf1;
    MFMailComposeViewController *mailComposer;
    IBOutlet UIImageView *editImage;
    PopUpViewController *wb;
    IBOutlet UITextField *titleField;
    BOOL isSendPressed;

}
- (IBAction)actionUseCameraRoll:(id)sender;

-(IBAction)goToHomePage:(id)sender;
-(IBAction)returnKeyPressed:(UITextField*)sender;
-(IBAction)titleEditingStarted:(UITextField*)sender;

@property (nonatomic, assign) BOOL isCameraShown;
@property (retain, nonatomic) UIPopoverController *popover;
@end
