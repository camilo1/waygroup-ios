//
//  UserLoginViewController.h
//  WayGroup
//
//  Created by Apnovator-Prakshi on 12/30/14.
//  Copyright (c) 2014 Apnovator-Prakshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserLoginViewController : UIViewController
{
    IBOutlet UILabel *username;
    IBOutlet UILabel *password1;
}
-(IBAction)back:(id)sender;

@property(nonatomic,strong) NSString *tf1Str;
@property(nonatomic,strong) NSString *tf2Str;
@end
